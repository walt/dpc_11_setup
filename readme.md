# Setting up the Hitec DPC-11 Servo Programmer
For some reason, there are a number of problems with setting up the DPC-11 on Windows 10 and these problems are not addressed by the install guide. (x64 only)
Errors:
- Requires .NET Framework version 2.0.50727
- Wrong drivers installed

Includes the needed drivers and .cat file.

**See 'dpc_11_setup.pdf' for help** 
